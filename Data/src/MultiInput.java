import java.util.Arrays;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.util.TransferFunctionType;

/**
*
* @author Karol
*/


public class MultiInput {   


	
	public static void main(String[] args) {

		
		// create multi layer perceptron
		MultiLayerPerceptron myMlPerceptron = new MultiLayerPerceptron(2, 3, 1);
		
		myMlPerceptron.getLearningRule().setMaxError(0.001);//0-1
		myMlPerceptron.getLearningRule().setLearningRate(1);//0-1
		myMlPerceptron.getLearningRule().setMaxIterations(10000);
		

        
		 System.out.println("Uczenie sieci...");
		 
		DataSet trainingSet = DataSets.GetDataSet();
		
		myMlPerceptron.learn(trainingSet);
	
        System.out.println("Zakoncznie uczenia");

        System.out.println("Testowanie sieci");
        
		testNeuralNetwork(myMlPerceptron, trainingSet);
		
		

         myMlPerceptron.calculate();
          double[] networkOutput = myMlPerceptron.getOutput();

          double pred = networkOutput[0] * DataSets.dataMaxRealEstatePrice + DataSets.dataMinRealEstatePrice;
          System.out.println("Przewidywana cena nieruchomo�ci w III kwartale 2014 roku = "+ pred);
          
          double realValue = 6301.66;

          System.out.println("Porz�dana warto�� wynosi = " + realValue + " r�nica mi�dzy warto�ci� faktyczn� a przwidzian� wynosi " + (realValue - pred));
    
	}

	public static void testNeuralNetwork(NeuralNetwork nnet, DataSet tset) {

		for(DataSetRow dataRow : tset.getRows()) {

		nnet.setInput(dataRow.getInput());
		nnet.calculate();
		double[ ] networkOutput = nnet.getOutput();
		System.out.print("Input: " + Arrays.toString(dataRow.getInput()) );
		System.out.println(" Output: " + Arrays.toString(networkOutput) );

		}

		}
	

	

}