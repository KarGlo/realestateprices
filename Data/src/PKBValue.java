import java.util.Arrays;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.util.TransferFunctionType;

/**
 *
 * @author Karol
 */

public class PKBValue {

    /**
     * @param args the command line arguments
     */
    public static double PKBValuePredice() {
    	
    	MultiLayerPerceptron neuralNet = new MultiLayerPerceptron(4, 9, 1);

//    	MultiLayerPerceptron neuralNet = new MultiLayerPerceptron(4, 9, 1);
    	
       neuralNet.getLearningRule().setMaxError(0.001);//0-1
       neuralNet.getLearningRule().setLearningRate(0.7);//0-1
       neuralNet.getLearningRule().setMaxIterations(1000);
       

    	DataSet trainingSet = new DataSet(4, 1);
    	
        double[] data = {319039.9, 328122.2, 335218.7, 379469.1, 328171.3, 348116.4, 354011.0, 407057.8, 355350.8, 375547.6, 382274.4,
        		440409.5, 376241.0, 393902.3, 399102.9, 446648.1, 384582.7, 400741.8, 412872.4, 463855.4, 403121.2};
        
        
        double datamax = -9999;
        double datamin = 9999;
        for (int i = 0; i < data.length; i++) {

            if (data[i] > datamax) {
                datamax = data[i];
            }
            if (data[i] < datamin) {
                datamin = data[i];
            }
        }
        
        datamax = datamax * 1.2;
        datamin = datamin * 0.8;

        for (int i = 0; i < data.length - 5; i++) {
            System.out.println(data[i] + " " + data[i + 1] + " " + data[i + 2] + " " + data[i + 3] + "->" + data[i + 4]);
            trainingSet.addRow(new DataSetRow(new double[]{(data[i] - datamin) / datamax, (data[i + 1] - datamin) / datamax, (data[i + 2] - datamin) / datamax, (data[i + 3] - datamin) / datamax}, new double[]{(data[i + 4] - datamin) / datamax}));
        }
        
        int dataLength = data.length;
        double realValue = 418370.2;
        
        System.out.println("Uczenie sieci...");
        
     // create multi layer perceptron
        neuralNet.learn(trainingSet);
        
        System.out.println("Zakoncznie uczenia");

        System.out.println("Testowanie sieci");
        
        testNeuralNetwork(neuralNet, trainingSet);
        
        

//        TrainingSet testSet = new TrainingSet();
//        testSet.addElement(new TrainingElement(new double[]{(data[dataLength - 4] - datamin) / datamax, (data[dataLength - 3] - datamin) / datamax, (data[dataLength - 2] - datamin) / datamax, (data[dataLength - 1] - datamin) / datamax}));
        double middle = (data[dataLength - 4]  + data[dataLength - 3]  + data[dataLength - 2]  + data[dataLength - 1] ) / 4;
//        for (DataSetRow dataRow : trainingSet.getRows()) {
//            neuralNet.setInput(dataRow.getInput());
            neuralNet.calculate();
            double[] networkOutput = neuralNet.getOutput();
            //System.out.print("Input: " + testElement.getInput()+"\n");//to test
            //System.out.println(" Output: " + networkOutput);//to test
            double pred = (networkOutput[0]) * datamax + datamin;
            System.out.println("�rednie PKB z ostatnich 4 kwarta��w = " + middle + " PLN \nPrzewidywane PKB w pi�tym kwartale = "+ pred);
            System.out.println("Porz�dana warto�� wynosi = " + realValue + " r�nica mi�dzy warto�ci� faktyczn� a przwidzian� wynosi " + (realValue - pred));
            
            if (pred < middle) {
                System.out.println("\nPrzwidywany trend jest malej�cy\n");
            } else {
//                System.out.println("\nUważaj, przwidywany trend jest rosn�cy\n");
            }
 //       }
            return pred;
    }
    
    
    public static void testNeuralNetwork(NeuralNetwork nnet, DataSet testSet) {

    	for(DataSetRow dataRow : testSet.getRows()) {
    	nnet.setInput(dataRow.getInput());
    	nnet.calculate();
    	double[ ] networkOutput = nnet.getOutput();
    	System.out.print("Input: " + Arrays.toString(dataRow.getInput()) );
    	System.out.println(" Output: " + Arrays.toString(networkOutput) );
    	}

    	}
    
    
}
