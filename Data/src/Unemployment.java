import java.util.Arrays;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.util.TransferFunctionType;

/**
 *
 * @author Karol
 */

public class Unemployment {

    /**
     * @param args the command line arguments
     */
    public static double UnemploymentPredice() {
    	
    	MultiLayerPerceptron neuralNet = new MultiLayerPerceptron(4, 9, 1);

//    	MultiLayerPerceptron neuralNet = new MultiLayerPerceptron(4, 9, 1);
    	
       neuralNet.getLearningRule().setMaxError(0.001);//0-1
       neuralNet.getLearningRule().setLearningRate(0.7);//0-1
       neuralNet.getLearningRule().setMaxIterations(100);
       

    	DataSet trainingSet = new DataSet(4, 1);
        double[] data = {8.3, 7.9, 8.1, 8.5, 10.6, 9.5, 9.1, 9.3, 10,0, 9.5, 9.3,
            9.7, 10.5, 9.9, 9.9, 10.1, 11.3, 10.4, 9.8, 9.8, 10.6};
			
        
        double datamax = -9999;
        double datamin = 9999;
        for (int i = 0; i < data.length; i++) {

            if (data[i] > datamax) {
                datamax = data[i];
            }
            if (data[i] < datamin) {
                datamin = data[i];
            }
        }
        
        datamax = datamax * 1.2;
        datamin = datamin * 0.8;

        for (int i = 0; i < data.length - 5; i++) {
            System.out.println(data[i] + " " + data[i + 1] + " " + data[i + 2] + " " + data[i + 3] + "->" + data[i + 4]);
            trainingSet.addRow(new DataSetRow(new double[]{(data[i] - datamin) / datamax, (data[i + 1] - datamin) / datamax, (data[i + 2] - datamin) / datamax, (data[i + 3] - datamin) / datamax}, new double[]{(data[i + 4] - datamin) / datamax}));
        }
        
        int dataLength = data.length;
        double realValue = 9.1;
        
        System.out.println("Uczenie sieci...");
        
     // create multi layer perceptron
        neuralNet.learn(trainingSet);
        
        System.out.println("Zakoncznie uczenia");

        System.out.println("Testowanie sieci");
        
        testNeuralNetwork(neuralNet, trainingSet);
        
        

//        TrainingSet testSet = new TrainingSet();
//        testSet.addElement(new TrainingElement(new double[]{(data[dataLength - 4] - datamin) / datamax, (data[dataLength - 3] - datamin) / datamax, (data[dataLength - 2] - datamin) / datamax, (data[dataLength - 1] - datamin) / datamax}));
        double middle = (data[dataLength - 4]  + data[dataLength - 3]  + data[dataLength - 2]  + data[dataLength - 1] ) / 4;
//        for (DataSetRow dataRow : trainingSet.getRows()) {
//            neuralNet.setInput(dataRow.getInput());
            neuralNet.calculate();
            double[] networkOutput = neuralNet.getOutput();
            //System.out.print("Input: " + testElement.getInput()+"\n");//to test
            //System.out.println(" Output: " + networkOutput);//to test
            double pred = (networkOutput[0]) * datamax + datamin;
            System.out.println("�rednie bezrobocie z ostatnich 4 kwarta��w = " + middle + " PLN \nPrzewidywane bezrobocie w pi�tym kwartale = "+ pred);
            System.out.println("Porz�dana warto�� wynosi = " + realValue + " r�nica mi�dzy warto�ci� faktyczn� a przwidzian� wynosi " + (realValue - pred));
            
            if (pred < middle) {
                System.out.println("\nPrzwidywany trend jest malejej�cy\n");
            } else {
                System.out.println("\nUwa�aj, przwidywany trend jest rosn�cy\n");
            }
 //       }
           
           return pred;

    }
    
    
    public static void testNeuralNetwork(NeuralNetwork nnet, DataSet testSet) {

    	for(DataSetRow dataRow : testSet.getRows()) {
    	nnet.setInput(dataRow.getInput());
    	nnet.calculate();
    	double[ ] networkOutput = nnet.getOutput();
    	System.out.print("Input: " + Arrays.toString(dataRow.getInput()) );
    	System.out.println(" Output: " + Arrays.toString(networkOutput) );
    	}

    	}
    
    
}
