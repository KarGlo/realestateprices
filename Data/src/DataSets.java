import java.util.Arrays;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.util.TrainingSetImport;
import org.neuroph.util.TransferFunctionType;


public class DataSets {
	

	public static double SetMinValue(double[] data){
		double datamin = 9999;    
        for (int i = 0; i < data.length; i++) {
            if (data[i] < datamin) {
                datamin = data[i];
            }
        }
        return datamin;
	}
	
	public static double SetMaxValue(double[] data){
		double datamax = -9999;
        for (int i = 0; i < data.length; i++) {

            if (data[i] > datamax) {
                datamax = data[i];
            }
        }
        return datamax;
	}
	
static double dataUnemploymentPredict = Unemployment.UnemploymentPredice();	
static double dataPKBPredict = PKBValue.PKBValuePredice();
static double dataRealEstatePricesPredict = RealEstatePrices.RealEstatePricesPredice();
	
static double[] dataUnemployment = {8.3, 7.9, 8.1, 8.5, 10.6, 9.5, 9.1, 9.3, 10,0, 9.5, 9.3,
            9.7, 10.5, 9.9, 9.9, 10.1, 11.3, 10.4, 9.8, 9.8, 10.6, dataUnemploymentPredict};
    
static double[] dataPKB = {319039.9, 328122.2, 335218.7, 379469.1, 328171.3, 348116.4, 354011.0, 407057.8, 355350.8, 375547.6, 382274.4,
    		440409.5, 376241.0, 393902.3, 399102.9, 446648.1, 384582.7, 400741.8, 412872.4, 463855.4, 403121.2, dataPKBPredict};

static double[] dataRealEstatePrices = {6625.56, 6521.75, 6502.06, 6629.21, 6842, 6785.09, 6683.95, 6564.41, 
    6868.59, 6789.42, 6686.22, 6486.5, 6207.76, 6298.58, 6103.61, 5931.07, 6031.5, 6165.58, 6227.26, 6314.23, 6182.02, dataRealEstatePricesPredict};
    
static int dataUnemploymentLength = dataUnemployment.length;
static int dataPKBLength = dataPKB.length;
static int dataRealEstatePricesLength = dataRealEstatePrices.length;
    
public static double dataMaxUnemployment = SetMaxValue(dataUnemployment) * 1.2;
public static double dataMinUnemployment = SetMinValue(dataUnemployment)* 0.8;
    
public static double dataMaxdataPKB = SetMaxValue(dataPKB) * 1.2;
public static double dataMindataPKB = SetMinValue(dataPKB)* 0.8;

public static double dataMaxRealEstatePrice = SetMaxValue(dataRealEstatePrices) * 1.2;
public static double dataMinRealEstatePrice = SetMinValue(dataRealEstatePrices)* 0.8;

	
	
    public static DataSet GetDataSet(){
    	
    	    	
		DataSet trainingSet = new DataSet(2, 1);
		
		trainingSet.addRow(new DataSetRow(new double[] {  (8.3 - dataMinUnemployment) / dataMaxUnemployment, (319039.9 - dataMindataPKB) /dataMaxdataPKB  }, new double[] { (6625.56 - dataMinRealEstatePrice) / dataMaxRealEstatePrice}));
		trainingSet.addRow(new DataSetRow(new double[] {  (7.9 - dataMinUnemployment) / dataMaxUnemployment, (328122.2 - dataMindataPKB) /dataMaxdataPKB }, new double[] { (6521.75 - dataMinRealEstatePrice) / dataMaxRealEstatePrice }));
		trainingSet.addRow(new DataSetRow(new double[] {  ( 8.1 - dataMinUnemployment) / dataMaxUnemployment, (335218.7 - dataMindataPKB) /dataMaxdataPKB }, new double[] { (6502.06 - dataMinRealEstatePrice) / dataMaxRealEstatePrice }));
		trainingSet.addRow(new DataSetRow(new double[] {  ( 8.5 - dataMinUnemployment) / dataMaxUnemployment, (379469.1 - dataMindataPKB) /dataMaxdataPKB }, new double[] { (6629.21 - dataMinRealEstatePrice) / dataMaxRealEstatePrice }));
		trainingSet.addRow(new DataSetRow(new double[] {  ( 10.6 - dataMinUnemployment) / dataMaxUnemployment, (328171.3 - dataMindataPKB) /dataMaxdataPKB }, new double[] { (6842 - dataMinRealEstatePrice) / dataMaxRealEstatePrice }));
		trainingSet.addRow(new DataSetRow(new double[] {  ( 9.5 - dataMinUnemployment) / dataMaxUnemployment, (348116.4 - dataMindataPKB) /dataMaxdataPKB }, new double[] { (6785.09 - dataMinRealEstatePrice) / dataMaxRealEstatePrice }));
		trainingSet.addRow(new DataSetRow(new double[] {  ( 9.1 - dataMinUnemployment) / dataMaxUnemployment, (354011.0 - dataMindataPKB) /dataMaxdataPKB }, new double[] { (6683.95 - dataMinRealEstatePrice) / dataMaxRealEstatePrice }));
		trainingSet.addRow(new DataSetRow(new double[] {  ( 9.3 - dataMinUnemployment) / dataMaxUnemployment, (407057.8 - dataMindataPKB) /dataMaxdataPKB }, new double[] { (6564.41 - dataMinRealEstatePrice) / dataMaxRealEstatePrice }));
		trainingSet.addRow(new DataSetRow(new double[] {  ( 10 - dataMinUnemployment) / dataMaxUnemployment, (355350.8 - dataMindataPKB) /dataMaxdataPKB }, new double[] { (6868.59 - dataMinRealEstatePrice) / dataMaxRealEstatePrice }));
		trainingSet.addRow(new DataSetRow(new double[] {  ( 9.5 - dataMinUnemployment) / dataMaxUnemployment, (375547.6 - dataMindataPKB) /dataMaxdataPKB }, new double[] { (6789.42 - dataMinRealEstatePrice) / dataMaxRealEstatePrice }));
		trainingSet.addRow(new DataSetRow(new double[] {  ( 9.3 - dataMinUnemployment) / dataMaxUnemployment, (382274.4 - dataMindataPKB) /dataMaxdataPKB }, new double[] { (6686.22 - dataMinRealEstatePrice) / dataMaxRealEstatePrice }));
		trainingSet.addRow(new DataSetRow(new double[] {  ( 9.7 - dataMinUnemployment) / dataMaxUnemployment, (440409.5 - dataMindataPKB) /dataMaxdataPKB }, new double[] { (6486.5 - dataMinRealEstatePrice) / dataMaxRealEstatePrice }));
		trainingSet.addRow(new DataSetRow(new double[] {  ( 10.5 - dataMinUnemployment) / dataMaxUnemployment, (376241.0 - dataMindataPKB) /dataMaxdataPKB }, new double[] { (6207.76 - dataMinRealEstatePrice) / dataMaxRealEstatePrice }));
		trainingSet.addRow(new DataSetRow(new double[] {  ( 9.9 - dataMinUnemployment) / dataMaxUnemployment, (393902.3 - dataMindataPKB) /dataMaxdataPKB }, new double[] { (6298.58 - dataMinRealEstatePrice) / dataMaxRealEstatePrice }));
		trainingSet.addRow(new DataSetRow(new double[] {  ( 9.9 - dataMinUnemployment) / dataMaxUnemployment, (399102.9 - dataMindataPKB) /dataMaxdataPKB }, new double[] { (6103.61 - dataMinRealEstatePrice) / dataMaxRealEstatePrice }));
		trainingSet.addRow(new DataSetRow(new double[] {  ( 10.1 - dataMinUnemployment) / dataMaxUnemployment, (446648.1 - dataMindataPKB) /dataMaxdataPKB }, new double[] { (5931.07 - dataMinRealEstatePrice) / dataMaxRealEstatePrice }));
		trainingSet.addRow(new DataSetRow(new double[] {  ( 11.3 - dataMinUnemployment) / dataMaxUnemployment, (384582.7 - dataMindataPKB) /dataMaxdataPKB }, new double[] { (6031.5 - dataMinRealEstatePrice) / dataMaxRealEstatePrice }));
		trainingSet.addRow(new DataSetRow(new double[] {  ( 10.4 - dataMinUnemployment) / dataMaxUnemployment, (400741.8 - dataMindataPKB) /dataMaxdataPKB }, new double[] { (6165.58 - dataMinRealEstatePrice) / dataMaxRealEstatePrice }));
		trainingSet.addRow(new DataSetRow(new double[] {  ( 9.8 - dataMinUnemployment) / dataMaxUnemployment, (412872.4 - dataMindataPKB) /dataMaxdataPKB }, new double[] { (6227.26 - dataMinRealEstatePrice) / dataMaxRealEstatePrice }));
		trainingSet.addRow(new DataSetRow(new double[] {  ( 9.8 - dataMinUnemployment) / dataMaxUnemployment, (463855.4 - dataMindataPKB) /dataMaxdataPKB }, new double[] { (6314.23 - dataMinRealEstatePrice) / dataMaxRealEstatePrice }));
		trainingSet.addRow(new DataSetRow(new double[] {  ( 10.6 - dataMinUnemployment) / dataMaxUnemployment, (403121.2 - dataMindataPKB) /dataMaxdataPKB }, new double[] { (6182.02 - dataMinRealEstatePrice) / dataMaxRealEstatePrice }));
		trainingSet.addRow(new DataSetRow(new double[] {  ( dataUnemploymentPredict - dataMinUnemployment) / dataMaxUnemployment, (dataPKBPredict - dataMindataPKB) /dataMaxdataPKB }, new double[] { (dataRealEstatePricesPredict - dataMinRealEstatePrice) / dataMaxRealEstatePrice }));
		
    	return trainingSet;
    }
   
	
}
